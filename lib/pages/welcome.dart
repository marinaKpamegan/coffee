import 'package:coffee/pages/auth/login/login.dart';
import 'package:coffee/pages/home.dart';
import 'package:coffee/widgets/button.dart';
import 'package:coffee/widgets/logo.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Welcome extends StatefulWidget {
  const Welcome({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Welcome();
  }
}

class _Welcome extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(15),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // login page content
              Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: OmbeLogo(
                    size: 50,
                  )),
              Text("Ombe",
                  style: TextStyle(
                      fontFamily: GoogleFonts.varela().fontFamily,
                      color: Colors.black,
                      fontSize: 35)),
              SizedBox(height: 10),
              Text("Coffee Shop App", style: TextStyle(fontSize: 15)),
              const Padding(
                padding: EdgeInsets.only(top: 35, bottom: 25),
                child: Text("Morning begins with \nOmbe Coffe",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center),
              ),
              CustomButton(
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => const Login())),
                  text: "Login from email",
                  icon: Icon(Icons.mail, color: Colors.white)),
              CustomButton(
                  onPressed: () {},
                  text: "Login with facebook",
                  icon: Icon(Icons.facebook, color: Colors.white),
                  color: Color(0xFF376ae8)),
              CustomButton(
                onPressed: () {},
                text: "Login with Google",
                icon: Icon(Icons.facebook, color: Colors.black),
                color: Colors.white,
                textColor: Colors.black,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
