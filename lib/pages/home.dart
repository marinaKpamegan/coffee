import 'package:coffee/pages/coffee_details.dart';
import 'package:coffee/utils/color_palette.dart';
import 'package:coffee/widgets/ombe_news.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  final ScrollController _scrollController = ScrollController();
  double containerHeight = 220;
  int activeIndex = 0;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      final index = (_scrollController.offset / 200).round();
      if (activeIndex != index) {
        setState(() {
          activeIndex = index;
        });
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text("Good morning", style: TextStyle(fontSize: 12)),
              const SizedBox(height: 2),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Marina", style: TextStyle(fontSize: 30)),
                  Row(
                    children: [
                      const Icon(Icons.notifications),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.menu)) // TODO change icon , on press open drawer
                    ],
                  )
                ],
              ),
              // search box
              Container(
                height: 50,
                margin: const EdgeInsets.symmetric(vertical: 20),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: const Color(0xfff7f7f7)),
                child: TextField(
                    decoration: const InputDecoration(
                        hintText: 'Search',
                        hintStyle: TextStyle(fontSize: 18),
                        suffixIcon: Icon(Icons.search),
                        border: InputBorder.none),
                    onTap: () {} // TODO on search text field tap
                    ),
              ),

              // main Listview
              SizedBox(
                height: 250,
                child: ListView.builder(
                  controller: _scrollController,
                    shrinkWrap: true,
                    itemCount: 10,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      final isActive = index == activeIndex;

                      return SizedBox(
                        height: !isActive ? containerHeight : 280,
                        child: Transform.scale(
                          scale: isActive? 1.0 : 0.8,
                          child: Stack(
                            children: [
                              GestureDetector(
                                child: Container(
                                  margin: !isActive?  const EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 3) : const EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 4),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: ColorPalette.primary.getColor,
                                      boxShadow: [
                                       isActive? BoxShadow(
                                          color: ColorPalette.primary.getColor.withOpacity(0.5),
                                          spreadRadius: 8,
                                          blurRadius: 8,
                                          offset: const Offset(0, 3), // changes position of shadow
                                        ) : const BoxShadow(),
                                      ]
                                  ),
                                  height: 200,
                                  width: 170,
                                  alignment: Alignment.bottomLeft,
                                  child: Container(
                                    margin: const EdgeInsets.only(bottom: 20, left: 8),
                                    child: SizedBox(
                                      height: 60,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          const Padding(padding: EdgeInsets.only(bottom: 3), child: Text("Creamy ice coffee", style: TextStyle(fontSize: 18, color: Colors.white))),
                                          //SizedBox(height: 5),
                                          SizedBox(
                                              height: 30,
                                              width: 100,
                                              child: Row(
                                                // crossAxisAlignment: CrossAxisAlignment.baseline,
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: const [
                                                  // Text("", style: TextStyle(fontSize: 20, color: Colors.white)),
                                                  Text("\$5,8", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25, color: Colors.white)),
                                                  Text("\$8,0", style: TextStyle(fontSize: 18, color: Colors.white))
                                                ],
                                              ))
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                          const DetailPage()));
                                },
                              ),
                              Positioned(
                                  bottom: 130,
                                  child: Image.asset(
                                      "assets/images/coffee/ice-coffee.png",
                                      width: 150)),
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              const Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Text(
                  "Categories",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              // Listview
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                height: 90,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 2 - 15,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 15),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: ColorPalette.greyLight.getColor,
                              offset: const Offset(
                                0.0,
                                0.0,
                              ),
                              blurRadius: 5.0,
                              spreadRadius: 1.0,
                            ),
                          ]),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.coffee_outlined,
                            color: ColorPalette.primary.getColor,
                            size: 35,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Beverages",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "47 Menus",
                                style: TextStyle(
                                    color: ColorPalette.primary.getColor,
                                    fontSize: 10),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 2 - 15,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 15),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: ColorPalette.greyLight.getColor,
                              offset: const Offset(
                                0.0,
                                0.0,
                              ),
                              blurRadius: 5.0,
                              spreadRadius: 1.0,
                            ),
                          ]),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.fastfood_outlined,
                            color: ColorPalette.primary.getColor,
                            size: 35,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Foods",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "31 Menus",
                                style: TextStyle(
                                    color: ColorPalette.primary.getColor,
                                    fontSize: 10),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              // last part : news TODO actualités
              Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text("Features beverages",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text("More")
                        ]),
                    const SizedBox(height: 30),
                    ListView(
                      shrinkWrap: true,
                      children: const [
                        OmbeNews(),
                        OmbeNews(),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

