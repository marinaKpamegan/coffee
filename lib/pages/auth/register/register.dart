import 'package:coffee/utils/color_palette.dart';
import 'package:coffee/widgets/button.dart';
import 'package:coffee/widgets/logo_full.dart';
import 'package:flutter/material.dart';

import '../../home.dart';

class Register extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Register();
  }
}

class _Register extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            padding: const EdgeInsets.all(15),
            child: Form(
              child: Column(
                children: [
                  const OmbeLogoFull(),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text("Create an account",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold)),
                        SizedBox(height: 20),
                        Text(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                            overflow: TextOverflow.clip,
                            style: TextStyle(fontSize: 15)),
                      ],
                    ),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      label: Padding(
                          padding: EdgeInsets.only(bottom: 25),
                          child: Text("Username")),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      label: Padding(
                          padding: EdgeInsets.only(bottom: 25),
                          child: Text("Email")),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  TextFormField(
                    obscureText: true,
                    decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        label: Padding(
                            padding: EdgeInsets.only(bottom: 25),
                            child: Text("Password")),
                        suffixIcon: Icon(
                          Icons.remove_red_eye_rounded,
                          color: Color(0xff03764d),
                        )),
                  ),
                  const SizedBox(height: 10),
                  CustomButton(
                      onPressed: () => Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (BuildContext context) => Home())),
                      text: "SIGN UP",
                      color: Colors.grey),
                  const SizedBox(height: 5),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("By typping \"Sign in\" you accept our "),
                        Text("terms and conditions",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: ColorPalette.primary.getColor))
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
