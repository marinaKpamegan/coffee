import 'package:coffee/pages/home.dart';
import 'package:coffee/widgets/button.dart';
import 'package:coffee/widgets/logo.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../utils/color_palette.dart';
import '../../../widgets/logo_full.dart';
import '../register/register.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Login();
  }
}

class _Login extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            padding: const EdgeInsets.all(15),
            child: Form(
              child: Column(
                children: [
                  const OmbeLogoFull(),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text("Sign in",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold)),
                        SizedBox(height: 20),
                        Text(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                            overflow: TextOverflow.clip,
                            style: TextStyle(fontSize: 15)),
                      ],
                    ),
                  ),

                  TextFormField(
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      label: Padding(
                          padding: EdgeInsets.only(bottom: 25),
                          child: Text("Username")),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  TextFormField(
                    obscureText: true,
                    decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        label: Padding(
                            padding: EdgeInsets.only(bottom: 25),
                            child: Text("Password")),
                        suffixIcon: Icon(
                          Icons.remove_red_eye_rounded,
                          color: Color(0xff03764d),
                        )),
                  ),
                  const SizedBox(height: 10),

                  CustomButton(onPressed: () {}, text: "LOGIN"),
                  const SizedBox(height: 5),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Forgot password ?"),
                        SizedBox(width: 5),
                        Text("Reset here",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: ColorPalette.primary.getColor))
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  // open login page then
                  const Text("Don't have an account ?"),
                  CustomButton(
                    onPressed: ()=> Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => Register())),
                    text: "CREATE AN ACCOUNT",
                    color: ColorPalette.warning.getColor,
                    textColor: Colors.black,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
