import 'package:coffee/utils/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

import '../utils/globals.dart';

class Tracking extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Tracking();
  }
}

class _Tracking extends State<Tracking> {
  @override
  Widget build(BuildContext context) {
    double MIDDLESIZE = MediaQuery.of(context).size.height / 2;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: const Text(
          "Tracking orders",
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
        ),
        // leading: IconButton(
        //     onPressed: () {
        //       Navigator.pop(context);
        //     },
        //     icon: const Icon(Icons.arrow_back)),
        elevation: 0,
      ),
      body: SafeArea(
          child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            // the map
            // TODO white map
            SizedBox(
              height: MIDDLESIZE,
              width: MediaQuery.of(context).size.width,
              child: FlutterMap(
                options: MapOptions(
                  center: LatLng(51.509364, -0.128928),
                  zoom: 9.2,
                ),
                nonRotatedChildren: [
                  AttributionWidget.defaultWidget(
                    source: 'OpenStreetMap contributors',
                    onSourceTapped: null,
                  ),
                ],
                layers: [
                  TileLayerOptions(
                      urlTemplate:
                          "https://api.mapbox.com/styles/v1/evidya/ckwygmau7015a14o74mjnqlz7/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiZXZpZHlhIiwiYSI6ImNrdmRmbnhlbTJyenAydG9rdWVocmMyaXMifQ.lVjNWIz5FYzOMm4v8wIEdw",
                      additionalOptions: {
                        'accessToken': ACCESS_TOKEN,
                        'id': 'mapbox.mapbox-streets-v8'
                      }
                  ),
                ],
              ),
            ),
            Positioned(
                bottom: 0,
                // grey container
                child: Container(
                  height: MIDDLESIZE - 50,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: ColorPalette.primary.getColor,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 80,
                            padding: EdgeInsets.only(left: 15),
                            child: Row(
                              children: [
                                const CircleAvatar(
                                    backgroundColor: Colors.white, radius: 30),
                                SizedBox(width: 15),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text("Mr Shandy",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15,
                                            color: Colors.white)),
                                    SizedBox(height: 5),
                                    Text("ID 2445556",
                                        style: TextStyle(color: Colors.white)),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 15),
                            child: Row(
                              children: [
                                CircleAvatar(
                                    backgroundColor:
                                        ColorPalette.primaryLight.getColor,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.phone_outlined,
                                        color: Colors.white,
                                      ),
                                    )),
                                const SizedBox(width: 8),
                                CircleAvatar(
                                    child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.message_outlined,
                                            color: Colors.white)),
                                    backgroundColor:
                                        ColorPalette.primaryLight.getColor)
                              ],
                            ),
                          )
                        ],
                      ),

                      // white container at bottom
                      Container(
                        height: MIDDLESIZE - 130,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 15),
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30)),
                        ),
                        child: Column(
                          children: [
                            // little grey bar
                            Container(
                              height: 5,
                              width: 50,
                              decoration: BoxDecoration(
                                  color: ColorPalette.greyLight.getColor,
                                  borderRadius: BorderRadius.circular(20)),
                            ),
                            SizedBox(height: 20), // space
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // pin + text 1
                                  Row(
                                    children: [
                                      CircleAvatar(
                                          backgroundColor: ColorPalette
                                              .primaryLight2.getColor,
                                          child: Icon(Icons.pin_drop_outlined,
                                              color: ColorPalette
                                                  .primary.getColor)),
                                      SizedBox(width: 20),
                                      Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Text("Sweet Corner St.",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold)),
                                          SizedBox(height: 8),
                                          Text("Franklin Avenue",
                                              style: TextStyle(fontSize: 15)),
                                        ],
                                      )
                                    ],
                                  ),

                                  // dashed line
                                  Padding(
                                      padding: EdgeInsets.only(left: 20),
                                      child: Dash(
                                          direction: Axis.vertical,
                                          length: 50,
                                          dashColor: Colors.grey)),

                                  // pin + text 2
                                  Row(
                                    children: [
                                      const CircleAvatar(
                                        backgroundColor: Colors.black,
                                        child: CircleAvatar(
                                            radius: 19,
                                            backgroundColor: Colors.white,
                                            child: Icon(
                                                Icons
                                                    .storefront, // TODO replace with right icon
                                                color: Colors.black)),
                                      ),
                                      const SizedBox(width: 20),
                                      Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: const [
                                          Text("Coffee Ombe Shop",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold)),
                                          SizedBox(height: 8),
                                          Text("Sent on 08:50 AM",
                                              style: TextStyle(fontSize: 15)),
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )),
          ],
        ),
      )),
    );
  }
}
