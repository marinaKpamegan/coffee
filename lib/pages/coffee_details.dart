import 'package:coffee/pages/tracking.dart';
import 'package:coffee/utils/color_palette.dart';
import 'package:coffee/widgets/button.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({super.key});
  @override
  State<StatefulWidget> createState() {
    return _DetailPageState();
  }
}

class _DetailPageState extends State<DetailPage> {
  double sliderValue = 50;
  int orderNumber = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorPalette.primary.getColor,
        body: SafeArea(
            child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.arrow_back, color: Colors.white)),
                  const Text(
                    "Details",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 5),
                    child: IconButton(
                        onPressed: (){},
                        icon: const Icon(
                          Icons.shopping_bag_rounded,
                          color: Colors.white,
                        )),
                  )
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Image.asset("assets/images/coffee/ice-coffee.png",
                    width: 260),
              ),

              // the white box on bottom
              Positioned(
                  bottom: 0,
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    height: MediaQuery.of(context).size.height / 2,
                    width: MediaQuery.of(context).size.width,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25))),
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // little bar on te top
                        Container(
                          height: 5,
                          width: 50,
                          decoration: BoxDecoration(
                              color: ColorPalette.greyLight.getColor,
                              borderRadius: BorderRadius.circular(20)),
                        ),
                        // now essential content
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: Text("Ice Chocolate Coffee", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: const Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", overflow: TextOverflow.clip, style: TextStyle(fontSize: 19, color: Colors.grey))
                        ),
                        SizedBox(height: 10),

                        // TODO: create my own slider
                        Slider(
                          min: 0,
                          max: 100,
                          value: sliderValue,
                          divisions: 4,
                          activeColor: ColorPalette.primary.getColor,
                          onChanged: (value) {
                            setState(() {
                              sliderValue = value;
                            });
                          },
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: const [
                                Text("\$", style: TextStyle(fontSize: 20)),
                                Text("5.8", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25)),
                                Text("\$8.0", style: TextStyle(fontSize: 20, color: Colors.grey))
                              ],
                            ),
                            // increase - decrease buttons
                            Container(
                              width: 120,
                              height: 40,
                              decoration: BoxDecoration(
                                border: Border.all(),
                                borderRadius: BorderRadius.circular(25)
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  IconButton(onPressed: (){
                                    setState(() {
                                      if(orderNumber > 0){
                                        orderNumber--;
                                      }
                                    });
                                  }, icon: const Icon(Icons.minimize_sharp)),
                                  Text(orderNumber.toString(), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                  IconButton(onPressed: (){
                                    setState(() {
                                      orderNumber++;
                                    });
                                  }, icon: const Icon(Icons.add))
                                ],
                              ),
                            )
                          ],
                        ),
                        
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                            width: MediaQuery.of(context).size.width,
                            child: const Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", overflow: TextOverflow.clip, style: TextStyle(color: Colors.grey))
                        ),
                        // TODO custom more the button
                        CustomButton(onPressed: ()=>Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => Tracking())), text: "PLACE ORDER \$5.8")
                      ],
                    ),
                  )),

              // cicular floating element
              Positioned(
                  right: 20,
                  bottom: MediaQuery.of(context).size.height / 2 - 30,
                  child: CircleAvatar(
                    backgroundColor: ColorPalette.yellow.getColor,
                    radius: 25,
                    child: const Text("4.5",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)),
                  ))
            ],
          ),
        )));
  }
}
