import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          Expanded(child: ListView(
            padding: EdgeInsets.zero,
            children: [
              CircleAvatar(
                radius: 30,
              )
            ],
          ))
        ],
      ),
    );
  }

}