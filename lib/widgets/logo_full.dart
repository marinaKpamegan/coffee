import 'package:coffee/utils/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OmbeLogoFull extends StatelessWidget{
  final double size;
  final Color color;
  final Color textColor;

  const OmbeLogoFull({this.size = 28, this.color = const Color(0xff03764d), this.textColor = Colors.black});
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxHeight: 100,
        maxWidth: MediaQuery.of(context).size.width/2
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.coffee, size: size, color: color),
          SizedBox(width: 8),
          Text("Ombe",
              style: TextStyle(
                  fontFamily: GoogleFonts.varela().fontFamily,
                  color: textColor,
                  fontSize: size)),
        ],
      ),
    );
  }

}