import 'package:flutter/material.dart';

class OmbeNews extends StatelessWidget{
  const OmbeNews({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 100,
            width: 100,
            margin: const EdgeInsets.only(right: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: const DecorationImage(
                image: AssetImage("assets/images/coffee/background.jpeg"),
                fit: BoxFit.fill
              )
            ),
          ),
          Container(
            margin: const EdgeInsets.all(5),
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width /2
            ),
            child: Column(
              children: const [
                Text("Hot creamy chocolate latte", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16), overflow: TextOverflow.fade),
                SizedBox(height: 5),
                Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", overflow: TextOverflow.ellipsis, maxLines: 3, style: TextStyle(color: Colors.grey))
              ],
            )

          )
        ],
      ),
    );
  }

}