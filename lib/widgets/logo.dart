import 'package:coffee/utils/color_palette.dart';
import 'package:flutter/material.dart';

class OmbeLogo extends StatelessWidget{
  final double size;
  final Color color;

  const OmbeLogo({this.size = 24, this.color = const Color(0xff03764d)});
  @override
  Widget build(BuildContext context) {
    return Icon(Icons.coffee, size: size, color: color);
  }

}