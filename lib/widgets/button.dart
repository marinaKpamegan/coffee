
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget{
  final VoidCallback onPressed;
  final Color color;
  final Color textColor;
  final String text;
  final bool isDisabled;
  final Widget icon;


  CustomButton({required this.onPressed, required this.text, this.textColor = Colors.white, this.isDisabled = false, this.color = const Color(0xff03764d), this.icon = const SizedBox()});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: ElevatedButton.icon(
        onPressed: onPressed,
        icon: icon,
        label: Text(text, style: TextStyle(color: textColor, fontWeight: FontWeight.bold, fontSize: 16)),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          minimumSize: const Size.fromHeight(50),
          shape: const StadiumBorder(),
          shadowColor: color.withOpacity(0.5)
        ),// <-- Text
      ),
    );
  }

}