import 'dart:core';

import 'package:flutter/material.dart';

enum ColorPalette{
  primary, secondary, success, warning, info, danger, yellow, grey, primaryLight, greyLight, primaryLight2
}

extension ColorPaletteExt on ColorPalette{
  Color get getColor {

    switch (this) {

      case ColorPalette.primary:
        return const Color(0xff03764d);
      case ColorPalette.secondary:
        return const Color(0xFF376ae8);
      case ColorPalette.danger:
        return const Color(0xFFEF476F);
      case ColorPalette.success:
        return const Color(0xff6FD08C);
      case ColorPalette.warning:
        return const Color(0xfff6dbb9);
      case ColorPalette.primaryLight:
        return const Color(0xff3e9f74);
      case ColorPalette.primaryLight2:
        return const Color(0xffe0ebe7);
      case ColorPalette.yellow:
        return const Color(0xfff9892b);
      case ColorPalette.grey:
        return const Color(0xFF5f5f5f);
      case ColorPalette.greyLight:
        return const Color(0xFFEDECEC);
      default:
        return Colors.white;
    }
  }
}
